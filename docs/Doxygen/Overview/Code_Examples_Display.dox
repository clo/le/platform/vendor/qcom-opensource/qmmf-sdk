/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*!

@page Code_Examples_Display Display

This tutorial demonstrates how to build a C++ sample application
that can execute QMMF Display.

> __Note__: While this sample code does not do any error checking, it
> is strongly recommended that users check for errors when using the QMMF APIs.


@tableofcontents

________________________________________________________________________________
@section example_disp_YUV TestYUV

This is example code for display of 1 YUV video. \n

<BLOCKQUOTE>
Following headers are required to call Display APIs
\code{.cpp}
#include <qmmf-sdk/qmmf_display.h>
#include <qmmf_display_params.h>
using namespace qmmf::display;
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Create Client display instance and connect to Display Service
\code{.cpp}
Display* display_;
display_->Connect();
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Register Callbacks from the display service
\code{.cpp}
DisplayCb display_status_cb_;
display_status_cb_.EventCb = [&](DisplayEventType event_type,
                                 void* event_data,
                                 size_t event_data_size)
    { /* Handle incoming events from display */ };

display_status_cb_.VSyncCb = [&](int64_t time_stamp)
    { /* Handle VSync callback from display */ };
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Create Display based on Display type
\code{.cpp}
display_->CreateDisplay(DisplayType::kPrimary, display_status_cb_);
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
CreateSurface based on SurfaceConfig params
\code{.cpp}
SurfaceConfig surface_config;
uint32_t surface_id;
memset(&surface_config, 0x0, sizeof surface_config);
surface_config.width = 1920;
surface_config.height = 1080;
surface_config.format = SurfaceFormat::kFormatYCbCr420SemiPlanarVenus;
surface_config.buffer_count = 4;
surface_config.cache = 0;
surface_config.use_buffer = 0;
surface_config.z_order = 1;

display_->CreateSurface(surface_config, &surface_id);
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Create SurfaceData and Dequeue Surface Buffer based on surface_id
\code{.cpp}
SurfaceData* surface_data = new SurfaceData();
surface_data->surface_id = surface_id;

display_->DequeueSurfaceBuffer(surface_id,
                               surface_data->surface_buffer);
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
fread the src buffer based on width and height
from the file and assigns the params to surface_data \n
insert the surface data to surface_data_map map
\code{.cpp}
surface_data->file = fopen(/* Locatipon of YUV file*/);

/* fread the src buffer here */

surface_data->buffer_ready = 1;
surface_data->surface_param.src_rect = {0.0, 0.0,
    static_cast<float>(surface_config.width),
    static_cast<float>(surface_config.height)};
surface_data->surface_param.dst_rect = {0.0, 0.0,
    static_cast<float>(surface_config.width),
    static_cast<float>(surface_config.height)};
surface_data->surface_param.surface_blending =
    SurfaceBlending::kBlendingCoverage;
surface_data->surface_param.surface_flags.cursor = 0;
surface_data->surface_param.frame_rate = 20;
surface_data->surface_param.solid_fill_color = 0;
surface_data->surface_param.surface_transform.rotation = 0.0f;
surface_data->surface_param.surface_transform.flip_horizontal = 0;
surface_data->surface_param.surface_transform.flip_vertical = 0;

surface_data_.insert({surface_id, surface_data});
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Now then Display Surface is created and surface data has been assigned
Run the DisplayThread for some time
\code{.cpp}
running_ = 1;
/* while running_ is set to 1, DisplayThread will queue, dequeue
   and fread Buffers for all the surfaces*/
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Destroy the surface, close the file, delete surface data
\code{.cpp}
display_->DestroySurface(surface_data->surface_id);
fclose(surface_data->file);
delete surface_data;
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Destroy Display and disconnect from display service
\code{.cpp}
display_->DestroyDisplay(DisplayType::kPrimary);
display_->Disconnect();
\endcode
</BLOCKQUOTE>
\n
________________________________________________________________________________
@section example_disp_BGRA TestBGRA

This is example code for display of 1 RGB image. \n

<BLOCKQUOTE>
Following headers are required to call Display APIs
\code{.cpp}
#include <qmmf-sdk/qmmf_display.h>
#include <qmmf_display_params.h>
using namespace qmmf::display;
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Create client Display instance and connect to Display Service
\code{.cpp}
Display* display_;
display_->Connect();
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Register Callbacks from the display service
\code{.cpp}
DisplayCb display_status_cb_;
display_status_cb_.EventCb = [&](DisplayEventType event_type,
                                 void* event_data,
                                 size_t event_data_size)
    { /* Handle incoming events from display */ };

display_status_cb_.VSyncCb = [&](int64_t time_stamp)
    { /* Handle VSync callback from display */ };
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Create Display based on Display type
\code{.cpp}
display_->CreateDisplay(DisplayType::kPrimary, display_status_cb_);
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
CreateSurface based on SurfaceConfig params
\code{.cpp}
SurfaceConfig surface_config;
uint32_t surface_id;
memset(&surface_config, 0x0, sizeof surface_config);
surface_config.width = 352;
surface_config.height = 288;
surface_config.format = SurfaceFormat::kFormatBGRA8888;
surface_config.buffer_count = 4;
surface_config.cache = 0;
surface_config.use_buffer = 0;
surface_config.z_order = 1;

display_->CreateSurface(surface_config, &surface_id);
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Create SurfaceData and Dequeue Surface Buffer based on surface_id
\code{.cpp}
SurfaceData* surface_data = new SurfaceData();
surface_data->surface_id = surface_id;

display_->DequeueSurfaceBuffer(surface_id,
                               surface_data->surface_buffer);
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
fread the src buffer based on width and height
from the file and assigns the params to surface_data \n
insert the surface data to surface_data_map map
\code{.cpp}
surface_data->file = fopen(/* Locatipon of RGB file*/);

/* fread the src buffer here */

surface_data->buffer_ready = 1;
surface_data->surface_param.src_rect = {0.0, 0.0,
    static_cast<float>(surface_config.width),
    static_cast<float>(surface_config.height)};
surface_data->surface_param.dst_rect = {0.0, 0.0,
    static_cast<float>(surface_config.width),
    static_cast<float>(surface_config.height)};
surface_data->surface_param.surface_blending =
    SurfaceBlending::kBlendingCoverage;
surface_data->surface_param.surface_flags.cursor = 0;
surface_data->surface_param.frame_rate = 20;
surface_data->surface_param.solid_fill_color = 0;
surface_data->surface_param.surface_transform.rotation = 0.0f;
surface_data->surface_param.surface_transform.flip_horizontal = 0;
surface_data->surface_param.surface_transform.flip_vertical = 0;

surface_data_.insert({surface_id, surface_data});
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Now then Display Surface is created and surface data has been assigned
Run the DisplayThread for some time
\code{.cpp}
running_ = 1;
/* while running_ is set to 1, DisplayThread will queue, dequeue
   and fread Buffers for all the surfaces*/
\endcode
</BLOCKQUOTE>
\code{.cpp}

<BLOCKQUOTE>
Destroy the surface, close the file, delete surface data
\code{.cpp}
display_->DestroySurface(surface_data->surface_id);
fclose(surface_data->file);
delete surface_data;
\endcode
</BLOCKQUOTE>
\code{.cpp}

<BLOCKQUOTE>
Destroy Display and disconnect from display service
\code{.cpp}
display_->DestroyDisplay(DisplayType::kPrimary);
display_->Disconnect();
\endcode
</BLOCKQUOTE>
\n
________________________________________________________________________________
@section example_disp_RGB_rotation TestRGBAWithRotation

This is example code for display of 1 RGB image with 90 degree rotation.

<BLOCKQUOTE>
Following headers are required to call Display APIs
\code{.cpp}
#include <qmmf-sdk/qmmf_display.h>
#include <qmmf_display_params.h>
using namespace qmmf::display;
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Create client Display instance and connect to Display Service
\code{.cpp}
Display* display_;
display_->Connect();
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Register Callbacks from the display service
\code{.cpp}
DisplayCb display_status_cb_;
display_status_cb_.EventCb = [&](DisplayEventType event_type,
                                 void* event_data,
                                 size_t event_data_size)
    { /* Handle incoming events from display */ };

display_status_cb_.VSyncCb = [&](int64_t time_stamp)
    { /* Handle VSync callback from display */ };
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Create Display based on Display type
\code{.cpp}
display_->CreateDisplay(DisplayType::kPrimary, display_status_cb_);
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
CreateSurface based on SurfaceConfig params
Set the rotation flasg to 90 degrees
\code{.cpp}
SurfaceConfig surface_config;
uint32_t surface_id;
memset(&surface_config, 0x0, sizeof surface_config);
surface_config.width = 352;
surface_config.height = 288;
surface_config.format = SurfaceFormat::kFormatRGBA8888;
surface_config.buffer_count = 4;
surface_config.cache = 0;
surface_config.use_buffer = 0;
surface_config.z_order = 1;
// Rotation by 90 degree
surface_config.surface_transform.rotation = 90.0f;
surface_config.surface_transform.flip_horizontal = 0;
surface_config.surface_transform.flip_vertical = 0;

display_->CreateSurface(surface_config, &surface_id);
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Create SurfaceData and Dequeue Surface Buffer based on surface_id
\code{.cpp}
SurfaceData* surface_data = new SurfaceData();
surface_data->surface_id = surface_id;

display_->DequeueSurfaceBuffer(surface_id,
                               surface_data->surface_buffer);
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
fread the src buffer based on width and height
from the file and assigns the params to surface_data \n
insert the surface data to surface_data_map map
\code{.cpp}
surface_data->file = fopen(/* Locatipon of RGB file*/);

/* fread the src buffer here */

surface_data->buffer_ready = 1;
surface_data->surface_param.src_rect = {0.0, 0.0,
    static_cast<float>(surface_config.width),
static_cast<float>(surface_config.height)};
surface_data->surface_param.dst_rect = {0.0, 0.0,
    static_cast<float>(surface_config.width),
    static_cast<float>(surface_config.height)};
surface_data->surface_param.surface_blending =
    SurfaceBlending::kBlendingCoverage;
surface_data->surface_param.surface_flags.cursor = 0;
surface_data->surface_param.frame_rate = 20;
surface_data->surface_param.solid_fill_color = 0;
// surface param for rotation
surface_data->surface_param.surface_transform.rotation = 90.0f;
surface_data->surface_param.surface_transform.flip_horizontal = 0;
surface_data->surface_param.surface_transform.flip_vertical = 0;

surface_data_.insert({surface_id, surface_data});
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Now then Display Surface is created and surface data has been assigned
Run the DisplayThread for some time
\code{.cpp}
running_ = 1;
/* while running_ is set to 1, DisplayThread will queue, dequeue
   and fread Buffers for all the surfaces*/
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Destroy the surface, close the file, delete surface data
\code{.cpp}
display_->DestroySurface(surface_data->surface_id);
fclose(surface_data->file);
delete surface_data;
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Destroy Display and disconnect from display service
\code{.cpp}
display_->DestroyDisplay(DisplayType::kPrimary);
display_->Disconnect();
\endcode
</BLOCKQUOTE>
\n
________________________________________________________________________________
@section example_disp_1YUVexternal1RGBinternal Test1YUV_External_1RGB_InternalBuffer

This is example code for display of 1 YUV Video and 1 RGB image
where the YUV buffer is allocated by the test app and RBG buffer by
Display Service.

<BLOCKQUOTE>
Following headers are required to call Display APIs
\code{.cpp}
#include <qmmf-sdk/qmmf_display.h>
#include <qmmf_display_params.h>
using namespace qmmf::display;
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Create client Display instance and connect to Display Service
\code{.cpp}
Display* display_;
display_->Connect();
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Register Callbacks from the display service
\code{.cpp}
DisplayCb display_status_cb_;
display_status_cb_.EventCb = [&](DisplayEventType event_type,
                                 void* event_data,
                                 size_t event_data_size)
    { /* Handle incoming events from display */ };

display_status_cb_.VSyncCb = [&](int64_t time_stamp)
    { /* Handle VSync callback from display */ };
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Create Display based on Display type
\code{.cpp}
display_->CreateDisplay(DisplayType::kPrimary, display_status_cb_);
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Foth both formats do following:
- CreateSurface one-by-one.
- Based on the SurfaceConfig params and surface_id,
CreateSurface and set SurfaceData params.
- fread the src buffer from the surface_data->file and
assign the surface_data params

</BLOCKQUOTE>

<BLOCKQUOTE>
YUV Surface, the params must set <b> use_buffer </b> flag to 1 for external buffers
</BLOCKQUOTE>

<BLOCKQUOTE>
CreateSurface based on SurfaceConfig params
\code{.cpp}
SurfaceConfig surface_config;
uint32_t surface_id;
memset(&surface_config, 0x0, sizeof surface_config);
surface_config.width = 1920;
surface_config.height = 1080;
surface_config.format = SurfaceFormat::kFormatYCbCr420SemiPlanarVenus;
surface_config.buffer_count = 4;
surface_config.cache = 0;
// use_buffer is 1 for external buffers
surface_config.use_buffer = 1;
surface_config.z_order = 1;
ret = display_->CreateSurface(surface_config, &surface_id);
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Create SurfaceData
\code{.cpp}
SurfaceData* surface_data = new SurfaceData();
surface_data->surface_id = surface_id;
\endcode

</BLOCKQUOTE>
<BLOCKQUOTE>
<b> Allocate buffers </b> (External buffer allocation)
\code{.cpp}
std::vector<BufInfo*> buffers;

for (uint32_t i = 0; i < surface_config.buffer_count; i++) {
BufInfo* new_buf_info = new BufInfo();
new_buf_info->buffer_info.buffer_config.width = surface_config.width;
new_buf_info->buffer_info.buffer_config.height = surface_config.height;
new_buf_info->buffer_info.buffer_config.format =
    static_cast<LayerBufferFormat>(surface_config.format);
new_buf_info->buffer_info.buffer_config.buffer_count = 1;
new_buf_info->buffer_info.buffer_config.cache = surface_config.cache;
new_buf_info->buffer_info.alloc_buffer_info.fd = -1;
new_buf_info->buffer_info.alloc_buffer_info.stride = 0;
new_buf_info->buffer_info.alloc_buffer_info.size = 0;
new_buf_info->buf = nullptr;
buffers.push_back(new_buf_info);
ret = buffer_allocator_.AllocateBuffer(&new_buf_info->buffer_info);
}

buf_info_.insert({surface_id, buffers});
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Assigns the SurfaceData params based on buf_info_ 1st BufferInfo
\code{.cpp}
surface_data->file = fopen(/* Locatipon of YUV file*/);

auto bufinfo = buf_info_.find(surface_id);
BufInfo* new_buf_info = bufinfo->second.at(0);
BufferInfo* bufferinfo = &new_buf_info->buffer_info;
surface_data->surface_buffer.plane_info[0].ion_fd =
    bufferinfo->alloc_buffer_info.fd;
surface_data->surface_buffer.buf_id = bufferinfo->alloc_buffer_info.fd;
surface_data->surface_buffer.format =
    static_cast<SurfaceFormat>(bufferinfo->buffer_config.format);
surface_data->surface_buffer.plane_info[0].stride =
    bufferinfo->alloc_buffer_info.stride;
surface_data->surface_buffer.plane_info[0].size =
    bufferinfo->alloc_buffer_info.size;
surface_data->surface_buffer.plane_info[0].width =
    bufferinfo->buffer_config.width;
surface_data->surface_buffer.plane_info[0].height =
    bufferinfo->buffer_config.height;
surface_data->surface_buffer.plane_info[0].offset = 0;
surface_data->surface_buffer.plane_info[0].buf = new_buf_info->buf =
    mmap(nullptr, (size_t)surface_data->surface_buffer.plane_info[0].size,
         PROT_READ | PROT_WRITE, MAP_SHARED,
         surface_data->surface_buffer.plane_info[0].ion_fd, 0);
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
fread the src buffer from the surface_data->file and
assign the surface_data params
\code{.cpp}
surface_data->buffer_ready = 1;
// for external buffer allocation
surface_data->surface_buffer_index = 0;
surface_data->surface_param.src_rect = {0.0, 0.0,
    static_cast<float>(surface_config.width),
    static_cast<float>(surface_config.height)};
surface_data->surface_param.dst_rect = {0.0, 0.0,
    static_cast<float>(surface_config.width),
    static_cast<float>(surface_config.height)};
surface_data->surface_param.surface_blending =
    SurfaceBlending::kBlendingCoverage;
surface_data->surface_param.surface_flags.cursor = 0;
surface_data->surface_param.frame_rate = 20;
surface_data->surface_param.solid_fill_color = 0;
surface_data->surface_param.surface_transform.rotation = 0.0f;
surface_data->surface_param.surface_transform.flip_horizontal = 0;
surface_data->surface_param.surface_transform.flip_vertical = 0;

surface_data_.insert({surface_id, surface_data});
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
RGB Surface, the params must set use_buffer flag to 0 for internal buffers
</BLOCKQUOTE>

<BLOCKQUOTE>
CreateSurface based on SurfaceConfig params
\code{.cpp}
memset(&surface_config, 0x0, sizeof surface_config);
surface_config.width = 352;
surface_config.height = 288;
surface_config.format = SurfaceFormat::kFormatBGRA8888;
surface_config.buffer_count = 4;
surface_config.cache = 0;
// use_buffer = 0 for internal buffers
surface_config.use_buffer = 0;
surface_config.z_order = 2;
ret = display_->CreateSurface(surface_config, &surface_id);
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Create SurfaceData and Dequeue Surface Buffer based on surface_id
\code{.cpp}
SurfaceData* surface_data = new SurfaceData();
surface_data->surface_id = surface_id;
display_->DequeueSurfaceBuffer(surface_id,
                               surface_data->surface_buffer);
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
fread the src buffer based on width and height
from the file and assigns the params to surface_data
\code{.cpp}
surface_data->file = fopen(/* Locatipon of RGB file*/);

surface_data->buffer_ready = 1;
surface_data->surface_param.src_rect = {0.0, 0.0,
    static_cast<float>(surface_config.width),
    static_cast<float>(surface_config.height)};
surface_data->surface_param.dst_rect = {0.0, 0.0,
    static_cast<float>(surface_config.width),
    static_cast<float>(surface_config.height)};
surface_data->surface_param.surface_blending =
    SurfaceBlending::kBlendingCoverage;
surface_data->surface_param.surface_flags.cursor = 0;
surface_data->surface_param.frame_rate = 20;
surface_data->surface_param.solid_fill_color = 0;
surface_data->surface_param.surface_transform.rotation = 0.0f;
surface_data->surface_param.surface_transform.flip_horizontal = 0;
surface_data->surface_param.surface_transform.flip_vertical = 0;

surface_data_.insert({surface_id, surface_data});
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Now then both the surfaces are created and surface data has been assigned
Run the DisplayThread for some time
\code{.cpp}
running_ = 1;
/* while running_ is set to 1, DisplayThread will queue, dequeue
   and fread Buffers for all the surfaces*/
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
For each surface, Destroy the surface, close the file,
delete surface data
\code{.cpp}
display_->DestroySurface(surface_data->surface_id);
fclose(surface_data->file);
delete surface_data;
\endcode
</BLOCKQUOTE>

<BLOCKQUOTE>
Destroy Display and disconnect from display service
\code{.cpp}
display_->DestroyDisplay(DisplayType::kPrimary);
display_->Disconnect();
\endcode
</BLOCKQUOTE>

\endcode
\n

________________________________________________________________________________

*/