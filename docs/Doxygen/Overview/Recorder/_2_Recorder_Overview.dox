/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*!

@page Recorder_Overview Overview

__Recorder__ :

Following are the major features supported by QMMF-SDK Recorder:
- Video and audio recording, both raw and encoded formats
- Supports linked video tracks
- JPEG, RAW and YUV snapshots
- Support for custom client plugins
- Post processing pipe for both video and snapshot utilizing plugins
- Capturing multiple images in succession (Burst capture)
- Capturing images with zero shutter lag
- Continuously capturing images until cancelled

__Basic concepts__ :

- __Session__: A communication line between the client and service containing one or multiple Audio/Video tracks. All tracks within a session changes states (like start, stop, pause) together.
- __Track__: Stream of either audio or video data configured by the client and transmitted in buffers. The data buffers are taken from the audio or camera device respectably. Each track is associated with a session on its creation.
- __Linked Video Track__: A track that is taking its data frames not directly from the camera device but from a source track. The source track ID is given to the linked track during its creation. Additional rescalling may be applied if the resolution of the linked track is different from that of the source track.
- __Post Processing__: Pipe containing different plugins specified by the client at Video Track creation or Image Capture configuration. Those algorithms will be applied on incoming frames from the camera device.
- __Plugin__: Dynamically loadable library containing a specific algorithm that utilizes the interface defined in <qmmf_alg_plugin.h>. To write a plugin the client needs to follow the defined interface. Plugins can be both proprietary and open source.
- __Overlay__: Additional layer chosen by the client and applied over each frame from the camera device after post processing.


__Adding a plugin__ :

- Call GetSupportedPlugins API to get a list of supported plugins
- Check if the plugin you want is supported
- If the plugin is supported you can now create it with unique id.
- Next you need to update the Image Config container (ImageConfigParam)
with this tag QMMF_POSTPROCESS_PLUGIN, your plugin (PostprocPlugin) and entry position (first position is 0).

\code{.cpp}
qmmf::recorder::PostprocPlugin plugin;
SupportedPlugins supported_plugins;
recorder.GetSupportedPlugins(&supported_plugins);
for (auto const& plugin_info : supported_plugins) {
  if (plugin_info.name == "BayerLcac") {
    recorder.CreatePlugin(&plugin.uid, plugin_info);
    image_config.Update(QMMF_POSTPROCESS_PLUGIN, plugin, 0);
  }
}
\endcode

- After you are done delete the plugin.

\code{.cpp}
    recorder.DeletePlugin(plugin.uid);
\endcode

See @ref example_rec_10MPSnapshotWithLCAC and @ref example_rec_ContinuousSnapshotWithBayer for reference.

____
*/
